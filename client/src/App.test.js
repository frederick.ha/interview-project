import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

window.fetch = jest.fn(() => Promise.resolve({
  json: () => ({}),
  status: 200,
}));

describe('App', () => {
  describe('render', () => {
    it('renders without crashing', () => {
      const div = document.createElement('div');
      ReactDOM.render(<App />, div);
    });
  })
});
